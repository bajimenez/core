//
//  ViewController.swift
//  Core
//
//  Created by Brayan Jimenez on 23/1/18.
//  Copyright © 2018 Brayan Jimenez. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    
    let manageObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func savePerson(){
        let entityDescription = NSEntityDescription.entity(forEntityName: "Person", in: manageObjectContext)
        let person = Person(entity: entityDescription!, insertInto: manageObjectContext)
        
        person.adress = addressTextField.text ?? ""
        person.name = nameTextField.text ?? ""
        person.phone = phoneTextField.text ?? ""
        
        do {
            try manageObjectContext.save()
            clearFields()
        }
        catch {
            print("error")
        }
        
    }
    
    func clearFields(){
        addressTextField.text = ""
        phoneTextField.text = ""
        nameTextField.text = ""
    }
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        savePerson()
    }
    
    @IBAction func findButtonPressed(_ sender: Any) {
        if nameTextField.text == "" {
            findAll()
            return
            
        }
        else {
            findOne()
        }
    }
    
    func findAll(){
       // let entityDescription = NSEntityDescription.entity(forEntityName: "Person", in: manageObjectContext)
        let request:NSFetchRequest<Person> = Person.fetchRequest()
        do {
            let results = try(manageObjectContext.fetch(request as! NSFetchRequest<NSFetchRequestResult>))
            
            for result in results{
                let person = result as! Person
                print("Name: \(person.name ?? "") ", terminator: "")
                print("Address: \(person.adress ?? "") ", terminator: "")
                print("Phone: \(person.phone ?? "") ", terminator: "")
                print()
                print()
            }
        }
        catch {
            print("Error Finding People")
            
        }
    }
    
    func findOne(){
        let entityDescription = NSEntityDescription.entity(forEntityName: "Person", in: manageObjectContext)
       let request:NSFetchRequest<Person> = Person.fetchRequest()
        
        request.entity = entityDescription
        let predicate = NSPredicate(format: "name = %@", nameTextField.text!)
        request.predicate = predicate
        
        do {
            let results = try(manageObjectContext.fetch(request as! NSFetchRequest<NSFetchRequestResult>))
            
            if results.count > 0 {
                let match = results[0] as! Person
                addressTextField.text = match.adress
                phoneTextField.text = match.phone
                nameTextField.text = match.name
            }
            else {
                addressTextField.text = "n/a"
                phoneTextField.text = "n/a"
                nameTextField.text = "n/a"
            }
        }
        catch {
            print("Error Finding one")
            
        }
    }
}

